import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PexelsService} from "../../../services/http/pexels/pexels.service";
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {PexelsPhoto} from "../../../services/http/pexels/entities";

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss']
})
export class PresentationComponent implements OnInit, AfterViewInit {

  private readonly PHOTO_ID_LANDSCAPE: number = 4090111
  private readonly PHOTO_ID_PORTRAIT: number = 2670898

  public photo?: PexelsPhoto;

  @ViewChild('background', { static: false }) backgroundEl?: ElementRef;

  constructor(
    private responsive: BreakpointObserver,
    private pexelsService: PexelsService
  ) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.responsive.observe([Breakpoints.HandsetPortrait])
      .subscribe(result => {
        const photoId: number = (result.matches) ? this.PHOTO_ID_PORTRAIT : this.PHOTO_ID_LANDSCAPE
        this.loadPhoto(photoId);
      });
  }

  public loadPhoto(photoId: number) {
    this.pexelsService.getPhoto(photoId)
      .subscribe(photo => {
        this.photo = photo;
        (this.backgroundEl?.nativeElement).style.backgroundImage = `url(${photo.src.original})`;
      });
  }

}
