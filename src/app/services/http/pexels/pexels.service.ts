import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {Observable, Subject, takeUntil} from "rxjs";
import {PexelsPhoto} from "./entities";

@Injectable({
  providedIn: 'root'
})
export class PexelsService implements OnDestroy {

  private uri: string = environment.pexelsUri;
  private context: string  = '/v1';
  private apiKey: string = environment.pexelsApiKey;

  private readonly unsubscribe$ = new Subject<void>();
  private readonly httpHeaders: HttpHeaders = new HttpHeaders();
  constructor(private httpClient: HttpClient) {
    this.httpHeaders = this.httpHeaders.set("Authorization", this.apiKey);
  }
  public getPhoto(id: number): Observable<PexelsPhoto> {
    if (!id) throw new Error('"id" needed to "getPhoto" request!');
    const uri = `${this.uri}${this.context}/photos/${id}`;

    return this.httpClient.get<PexelsPhoto>(uri, { headers: this.httpHeaders })
      .pipe(takeUntil(this.unsubscribe$));
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
