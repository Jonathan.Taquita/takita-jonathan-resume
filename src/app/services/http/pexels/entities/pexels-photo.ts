import {PexelsSrc} from "./pexels-src";

export interface PexelsPhoto {
  id: number
  width: number
  height: number
  url: string
  photographer: string
  photographer_url: string
  photographer_id: number
  avg_color: string
  src: PexelsSrc
  liked: boolean
  alt: string
}
