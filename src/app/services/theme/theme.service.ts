import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Subject, takeUntil} from "rxjs";
import {ThemeEnum} from "./theme.enum";

@Injectable({
  providedIn: 'root'
})
export class ThemeService implements OnDestroy {

  private theme: BehaviorSubject<ThemeEnum> = new BehaviorSubject<ThemeEnum>(ThemeEnum.DARK);
  private unsubscribe$: Subject<void> = new Subject<void>();

  public setTheme(theme: ThemeEnum) { this.theme.next(theme); }

  constructor() {
    this.theme
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(value => {
        let htmlElement: HTMLHtmlElement = document.querySelector('html')!;
        htmlElement.setAttribute('data-theme', value);
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


}
