import { TestBed } from '@angular/core/testing';

import { ThemeService } from './theme.service';
import {ThemeEnum} from "./theme.enum";

describe('ThemeService', () => {
  let service: ThemeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThemeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('method - setTheme()', () => {
    it('should setter value', () => {
      service.setTheme(ThemeEnum.DARK);

      expect(service['theme'].getValue()).toEqual('dark')
    });
  });

});
